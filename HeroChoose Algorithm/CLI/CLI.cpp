// CLI.cpp : 기본 프로젝트 파일입니다.
#define _CRT_SECURE_NO_WARNINGS

#using <System.dll>

#include "stdafx.h"
#include <math.h>
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <opencv/highgui.h>
#include <opencv/cv.h>

using namespace System;
using namespace System::IO;
using namespace System::Security::Permissions;

public ref class Watcher
{
private:

	static const int M = 23;
	static const int E = 6;
	static const int AL = 5;


	int num_input;
	int num_hidden;
	int num_output;

	double **hidden_weight;
	double **output_weight;

	void Compute_Output(double input[], double hidden[], double output[]){
		for (int i = 0; i < num_hidden; i++){
			double sum = 0;

			for (int j = 0; j < num_input; j++){
				sum += input[j] * hidden_weight[i][j];

			}
			sum += hidden_weight[i][num_input];
			hidden[i] = 1 / (1 + exp(-sum));
		}
		for (int i = 0; i < num_output; i++){
			double sum = 0;

			for (int j = 0; j < num_hidden; j++){
				sum += hidden[j] * output_weight[i][j];
			}
			sum += output_weight[i][num_hidden];
			output[i] = 1 / (1 + exp(-sum));
		}
	}

	static int compare_image(IplImage *src_image, IplImage *dst_image) {

		double calc = 0;
		int match = 0;

		for (int i = 0; i < dst_image->widthStep; i += 3){
			for (int j = 0; j < dst_image->height; j++) {
				calc = (double)dst_image->imageData[i + j * dst_image->widthStep] / (double)src_image->imageData[i + j*dst_image->widthStep];
				if ((calc > 0.9) && (calc < 1.1)) {
					match++;
					calc = 0;
				}
				else {
					calc = 0;
				}
				calc = (double)dst_image->imageData[i + j * dst_image->widthStep + 1] / (double)src_image->imageData[i + j * dst_image->widthStep + 1];
				if ((calc > 0.9) && (calc < 1.1)) {
					match++;
					calc = 0;
				}
				else {
					calc = 0;
				}
				calc = (double)dst_image->imageData[i + j * dst_image->widthStep + 2] / (double)src_image->imageData[i + j * dst_image->widthStep + 2];
				if ((calc > 0.9) && (calc < 1.1)) {
					match++;
					calc = 0;
				}
				else {
					calc = 0;
				}
			}
		}
		return match;
	}

	static void OnRenamed(Object^ /*source*/, RenamedEventArgs^ e)
	{
		// Specify what is done when a file is renamed.
		Console::WriteLine("File: {0} renamed to {1}", e->OldFullPath, e->FullPath);
	}

public:
	
	static int *ae;
	static int *aa;

	// Define the event handlers.
	static void OnChanged(Object^ /*source*/, FileSystemEventArgs^ e)
	{
		

		ae = (int*)malloc(sizeof(int)* 6);
		aa = (int*)malloc(sizeof(int)* 5);
		
		double max = 0;

		IplImage* objects[M];
		IplImage* enemyheroes[E];
		IplImage* allyheroes[AL];
		IplImage* new_image;

		char buf[255];
		// Specify what is done when a file is changed, created, or deleted.
		Console::WriteLine("File: {0} {1}", e->FullPath, e->ChangeType);

		_sleep(100);
		

		sprintf_s(buf, "%s", e->FullPath);

		printf("%s\n", buf);

		_sleep(100);

		

		new_image = cvLoadImage(buf);

		

		for (int i = 0; i < E; i++) {
			enemyheroes[i] = cvCreateImage(cvSize(66, 41), 8, 3);
			cvSetImageROI(new_image, cvRect(368 + 160 * i, 238, 66, 41));
			cvCopy(new_image, enemyheroes[i]);
			cvResetImageROI(new_image);
		}

		for (int i = 0; i < AL; i++) {
			allyheroes[i] = cvCreateImage(cvSize(66, 41), 8, 3);
			cvSetImageROI(new_image, cvRect(528 + 160 * i, 495, 66, 41));
			cvCopy(new_image, allyheroes[i]);
			cvResetImageROI(new_image);
		}

		printf("df\n");

		objects[0] = cvLoadImage("c:/opencv/overwatch heroes/genzi.jpg", -1);
		objects[1] = cvLoadImage("c:/opencv/overwatch heroes/reaper.jpg", -1);
		objects[2] = cvLoadImage("c:/opencv/overwatch heroes/mcCree.jpg", -1);
		objects[3] = cvLoadImage("c:/opencv/overwatch heroes/soldier76.jpg", -1);
		objects[4] = cvLoadImage("c:/opencv/overwatch heroes/sombra.jpg", -1);
		objects[5] = cvLoadImage("c:/opencv/overwatch heroes/tracer.jpg", -1);
		objects[6] = cvLoadImage("c:/opencv/overwatch heroes/pharah.jpg", -1);
		objects[7] = cvLoadImage("c:/opencv/overwatch heroes/mei.jpg", -1);
		objects[8] = cvLoadImage("c:/opencv/overwatch heroes/bastion.jpg", -1);
		objects[9] = cvLoadImage("c:/opencv/overwatch heroes/widowmaker.jpg", -1);
		objects[10] = cvLoadImage("c:/opencv/overwatch heroes/junkrat.jpg", -1);
		objects[11] = cvLoadImage("c:/opencv/overwatch heroes/torbjorn.jpg", -1);
		objects[12] = cvLoadImage("c:/opencv/overwatch heroes/hanzo.jpg", -1);
		objects[13] = cvLoadImage("c:/opencv/overwatch heroes/dva.jpg", -1);
		objects[14] = cvLoadImage("c:/opencv/overwatch heroes/reinhardt.jpg", -1);
		objects[15] = cvLoadImage("c:/opencv/overwatch heroes/roadhog.jpg", -1);
		objects[16] = cvLoadImage("c:/opencv/overwatch heroes/winston.jpg", -1);
		objects[17] = cvLoadImage("c:/opencv/overwatch heroes/zarya.jpg", -1);
		objects[18] = cvLoadImage("c:/opencv/overwatch heroes/lucio.jpg", -1);
		objects[19] = cvLoadImage("c:/opencv/overwatch heroes/mercy.jpg", -1);
		objects[20] = cvLoadImage("c:/opencv/overwatch heroes/symmetra.jpg", -1);
		objects[21] = cvLoadImage("c:/opencv/overwatch heroes/ana.jpg", -1);
		objects[22] = cvLoadImage("c:/opencv/overwatch heroes/zenyatta.jpg", -1);



		


		for (int i = 0; i < E; i++) {
			for (int j = 0; j < M; j++) {
				printf("enemyheroes[%d]와 object[%d]와 대상의 유사도는 %d입니다. \n", i, j, compare_image(enemyheroes[i], objects[j]));
				if (max < compare_image(enemyheroes[i], objects[j])) {
					max = compare_image(enemyheroes[i], objects[j]);
				}
			}
			for (int j = 0; j < M; j++) {
				if (max == compare_image(enemyheroes[i], objects[j])){
					ae[i] = j;
				}
			}
			printf("enemyheroes[%d]와 가장 유사한 영웅은 바로 %d입니다.", i, ae[i]);
			max = 0;
			printf("\n");
		}
		
		for (int i = 0; i < AL; i++) {
			for (int j = 0; j < M; j++) {
				printf("allyheroes[%d]와 object[%d]와 대상의 유사도는 %d입니다. \n", i, j, compare_image(allyheroes[i], objects[j]));
				if (max < compare_image(allyheroes[i], objects[j])) {
					max = compare_image(allyheroes[i], objects[j]);
				}
			}
			for (int j = 0; j < M; j++) {
				if (max == compare_image(allyheroes[i], objects[j])){
					aa[i] = j;
				}
			}
			printf("allyheroes[%d]와 가장 유사한 영웅은 바로 %d입니다.", i, aa[i]);
			max = 0;
			printf("\n");
		}
		/*
		cvShowImage("image1", enemyheroes[0]);
		cvShowImage("image2", enemyheroes[1]);
		cvShowImage("image3", enemyheroes[2]);
		cvShowImage("image4", enemyheroes[3]);
		cvShowImage("image5", enemyheroes[4]);
		cvShowImage("image6", enemyheroes[5]);
		cvShowImage("cmp1", objects[ae[0]]);
		cvShowImage("cmp2", objects[ae[1]]);
		cvShowImage("cmp3", objects[ae[2]]);
		cvShowImage("cmp4", objects[ae[3]]);
		cvShowImage("cmp5", objects[ae[4]]);
		cvShowImage("cmp6", objects[ae[5]]);
		*/
		cvWaitKey(0);

		cvDestroyAllWindows();
		for (int i = 0; i < E; i++) {
			cvReleaseImage(&enemyheroes[i]);
		}
		for (int i = 0; i < M; i++) {
			cvReleaseImage(&objects[i]);
		}
		
		for (int i = 0; i < 280; i++) {
			buf[i] = NULL;
		}
		
	}

	[PermissionSet(SecurityAction::Demand, Name = "FullTrust")]
	int static run()
	{
		array<String^>^args = System::Environment::GetCommandLineArgs();
		/*
		// If a directory is not specified, exit program.
		if (args->Length != 2)
		{
			// Display the proper way to call the program.
			Console::WriteLine("Usage: Watcher.exe (directory)");
			return 0;
		}
		*/

		// Create a new FileSystemWatcher and set its properties.
		FileSystemWatcher^ watcher = gcnew FileSystemWatcher;

		watcher->Path = "C:\\Users\\Toshiba_Laptop\\Documents\\Overwatch\\ScreenShots\\Overwatch";

		/* Watch for changes in LastAccess and LastWrite times, and
		the renaming of files or directories. */
		watcher->NotifyFilter = static_cast<NotifyFilters>(NotifyFilters::LastAccess |
			NotifyFilters::LastWrite | NotifyFilters::FileName | NotifyFilters::DirectoryName);

		// Only watch text files.
		watcher->Filter = "*.jpg";

		// Add event handlers.
		
		watcher->Created += gcnew FileSystemEventHandler(Watcher::OnChanged);
		watcher->Renamed += gcnew RenamedEventHandler(Watcher::OnRenamed);

		// Begin watching.
		watcher->EnableRaisingEvents = true;

		// Wait for the user to quit the program.

		
		Console::WriteLine("게임 중 tab 키를 누르고 스크린 샷 버튼을 누르세요. 다음 Q를 누르면 기계학습을 시작합니다.");
		
		while (Console::Read() != 'q');
		
		return 0;
			
	}
	Watcher(int num_input, int num_hidden, int num_output){
		this->num_input = num_input;
		this->num_hidden = num_hidden;
		this->num_output = num_output;

		output_weight = new double*[num_output];
		for (int i = 0; i < num_output; i++){
			output_weight[i] = new double[num_hidden + 1];
		}
		hidden_weight = new double*[num_hidden];
		for (int i = 0; i < num_hidden; i++){
			hidden_weight[i] = new double[num_input + 1];
		}
	}

	~Watcher(){
		for (int i = 0; i < num_output; i++){
			delete[] output_weight[i];
		}
		for (int i = 0; i < num_hidden; i++){
			delete[] hidden_weight[i];
		}
		delete[] hidden_weight;
		delete[] output_weight;
	}

	void Test(double input[], double output[]){
		double *hidden = new double[num_hidden];

		Compute_Output(input, hidden, output);

		delete[] hidden;
	}

	void Train(int num_train, double learning_rate, double **input, double **target_output){
		int num_epoch = 0;
		int max_epoch = 10000;

		double *hidden = new double[num_hidden];
		double *hidden_derivative = new double[num_hidden];
		double *output = new double[num_output];
		double *output_derivative = new double[num_output];

		srand(0);
		for (int i = 0; i < num_hidden; i++){
			for (int j = 0; j < num_input + 1; j++){
				hidden_weight[i][j] = 0.2 * rand() / RAND_MAX - 0.1;
			}
		}
		for (int i = 0; i < num_output; i++){
			for (int j = 0; j < num_hidden + 1; j++){
				output_weight[i][j] = 0.2 * rand() / RAND_MAX - 0.1;
			}
		}

		do{
			double error = 0;

			for (int i = 0; i < num_train; i++){

				Compute_Output(input[i], hidden, output);

				// 출력미분값 계산
				for (int j = 0; j < num_output; j++){
					output_derivative[j] = learning_rate * (output[j] - target_output[i][j]) * (1 - output[j]) * output[j];
				}

				// 출력가중치 조정
				for (int j = 0; j < num_output; j++){
					for (int k = 0; k < num_hidden; k++){
						output_weight[j][k] -= output_derivative[j] * hidden[k];
					}
					output_weight[j][num_hidden] -= output_derivative[j];
				}

				// 은닉미분값 계산
				for (int j = 0; j < num_hidden; j++){
					double sum = 0;

					for (int k = 0; k < num_output; k++){
						sum += output_derivative[k] * output_weight[k][j];
					}
					hidden_derivative[j] = sum * (1 - hidden[j]) * hidden[j];
				}

				// 은닉가중치 조정
				for (int j = 0; j < num_hidden; j++){
					for (int k = 0; k < num_input; k++){
						hidden_weight[j][k] -= hidden_derivative[j] * input[i][k];
					}
					hidden_weight[j][num_input] -= hidden_derivative[j];
				}

				// 오차 계산
				for (int j = 0; j < num_output; j++){
					error += 0.5 * (output[j] - target_output[i][j]) * (output[j] - target_output[i][j]);
				}
			}
			if (num_epoch % 1000 == 0){
				printf("반복횟수: %d, 오차: %4f\n", num_epoch, error);
			}
		} while (num_epoch++ < max_epoch);

		delete[] hidden;
		delete[] output;
	}

};

int main() {
	

	int num_input = 12;
	int num_hidden = 20;
	int num_output = 23;
	int num_train = 100;

	double learning_rate = 0.01;

	double **input = new double*[num_train];
	double **target_output = new double*[num_train];

	double **testdata = new double*[];

	testdata[0] = new double[num_input];

	Watcher ^FNN = gcnew Watcher(num_input, num_hidden, num_output);

	for (int i = 0; i < num_train; i++){
		input[i] = new double[num_input];
		target_output[i] = new double[num_output];
	}

	double cmmip[23][70];
	
	char *Heroname[23] = { "겐지", "리퍼", "맥크리", "솔저76", "솜브라", "트레이서", "파라", "메이", "바스티온",
		"위도우메이커", "정크랫", "토르비욘", "한조", "디바", "라인하르트", "로드호그", "윈스턴", "자리야", "루시우",
		"메르시", "시메트라", "아나", "젠야타" };
	

	cmmip[0][0] = 0;	    cmmip[0][1] = 0;		cmmip[0][2] = 0;	cmmip[0][3] = 0;		cmmip[0][4] = 0;
	cmmip[0][5] = 0;		cmmip[0][6] = 0;		cmmip[0][7] = -0.5;	cmmip[0][8] = 1;		cmmip[0][9] = 1;
	cmmip[0][10] = -0.5;	cmmip[0][11] = -0.5;	cmmip[0][12] = 0;	cmmip[0][13] = -0.5;	cmmip[0][14] = 0;
	cmmip[0][15] = 0;	    cmmip[0][16] = -1;		cmmip[0][17] = -1;	cmmip[0][18] = 1;		cmmip[0][19] = 1;
	cmmip[0][20] = -0.5;	cmmip[0][21] = 0;		cmmip[0][22] = 1;	//적군 카운터

	cmmip[0][23] = 0;		cmmip[0][24] = 0;
	cmmip[0][25] = 0;	    cmmip[0][26] = 0;		cmmip[0][27] = 0;	cmmip[0][28] = 0;		cmmip[0][29] = 0;
	cmmip[0][30] = 0;	    cmmip[0][31] = 0;		cmmip[0][32] = 1;	cmmip[0][33] = 0;		cmmip[0][34] = 0;
	cmmip[0][35] = 0;	    cmmip[0][36] = 0;		cmmip[0][37] = 0;	cmmip[0][38] = 0;		cmmip[0][39] = 1;
	cmmip[0][40] = 1;	    cmmip[0][41] = 1;		cmmip[0][42] = 0;	cmmip[0][43] = 0;		cmmip[0][44] = 1;
	cmmip[0][45] = 1;	    //아군 매치업

	cmmip[0][46] = 0.3438;	cmmip[0][47] = 0.5;		cmmip[0][48] = 0.3784;	cmmip[0][49] = 0.45;	cmmip[0][50] = 0.3929;
	cmmip[0][51] = 0.5909;	cmmip[0][52] = 0.3929;	cmmip[0][53] = 0;		cmmip[0][54] = 0.6842;	cmmip[0][55] = 0.1111;
	cmmip[0][56] = 0.375;	cmmip[0][57] = 0;		cmmip[0][58] = 0.4815;	cmmip[0][59] = 0.4211;	cmmip[0][60] = 0.4524;
	cmmip[0][61] = 0.3667;	cmmip[0][62] = 0.4651;	cmmip[0][63] = 0.3929;	cmmip[0][64] = 0.4286;	cmmip[0][65] = 0.3;
	cmmip[0][66] = 0.3807;	cmmip[0][67] = 0.4909;	cmmip[0][68] = 0.5;		cmmip[0][69] = 0.4209;	//맵

	/////겐지

	cmmip[1][0] = 0;	    cmmip[1][1] = 0;		cmmip[1][2] = -1;	cmmip[1][3] = 0;		cmmip[1][4] = 1;
	cmmip[1][5] = 0;		cmmip[1][6] = -0.5;		cmmip[1][7] = 0.5;	cmmip[1][8] = 1;		cmmip[1][9] = 0;
	cmmip[1][10] = 0;		cmmip[1][11] = 0;		cmmip[1][12] = 0;	cmmip[1][13] = 0;		cmmip[1][14] = 1;
	cmmip[1][15] = 1;	    cmmip[1][16] = 1;		cmmip[1][17] = 0;	cmmip[1][18] = 0;		cmmip[1][19] = 0;
	cmmip[1][20] = 0;		cmmip[1][21] = 0.5;		cmmip[1][22] = 0;

	cmmip[1][23] = 0;		cmmip[1][24] = 0;
	cmmip[1][25] = 0;	    cmmip[1][26] = 0;		cmmip[1][27] = 0;	cmmip[1][28] = 0;		cmmip[1][29] = 0;
	cmmip[1][30] = 1;	    cmmip[1][31] = 0;		cmmip[1][32] = 0;	cmmip[1][33] = 0;		cmmip[1][34] = 0;
	cmmip[1][35] = 0;	    cmmip[1][36] = 0;		cmmip[1][37] = 0;	cmmip[1][38] = 0;		cmmip[1][39] = 1;
	cmmip[1][40] = 1;	    cmmip[1][41] = 1;		cmmip[1][42] = 0;	cmmip[1][43] = 0;		cmmip[1][44] = 1;
	cmmip[1][45] = 1;

	cmmip[1][46] = 0.3714;	cmmip[1][47] = 0.4324;	cmmip[1][48] = 0.4151;	cmmip[1][49] = 0.4386;	cmmip[1][50] = 0.24;
	cmmip[1][51] = 0.4667;	cmmip[1][52] = 0;		cmmip[1][53] = 0;		cmmip[1][54] = 0.5313;	cmmip[1][55] = 0.186;
	cmmip[1][56] = 0;		cmmip[1][57] = 0;		cmmip[1][58] = 0.4706;	cmmip[1][59] = 0.3636;	cmmip[1][60] = 0.4535;
	cmmip[1][61] = 0.4432;	cmmip[1][62] = 0.5517;	cmmip[1][63] = 0.4167;	cmmip[1][64] = 0.4667;	cmmip[1][65] = 0.5714;
	cmmip[1][66] = 0.4565;	cmmip[1][67] = 0.3952;	cmmip[1][68] = 0;		cmmip[1][69] = 0;

	/////리퍼

	cmmip[2][0] = 0;	    cmmip[2][1] = 1;		cmmip[2][2] = 0;	cmmip[2][3] = 0;		cmmip[2][4] = 0;
	cmmip[2][5] = 0;		cmmip[2][6] = 1;		cmmip[2][7] = 0.5;	cmmip[2][8] = 0;		cmmip[2][9] = 0;
	cmmip[2][10] = 0.5;		cmmip[2][11] = 0;		cmmip[2][12] = 0;	cmmip[2][13] = 0;		cmmip[2][14] = 0;
	cmmip[2][15] = 1;	    cmmip[2][16] = 1;		cmmip[2][17] = -1;	cmmip[2][18] = 1;		cmmip[2][19] = 0.5;
	cmmip[2][20] = 0;		cmmip[2][21] = 0;		cmmip[2][22] = 0;

	cmmip[2][23] = 0;		cmmip[2][24] = 0;
	cmmip[2][25] = 0;	    cmmip[2][26] = 0;		cmmip[2][27] = 0;	cmmip[2][28] = 0;		cmmip[2][29] = 0;
	cmmip[2][30] = 0;	    cmmip[2][31] = 0;		cmmip[2][32] = 0;	cmmip[2][33] = 0;		cmmip[2][34] = 0;
	cmmip[2][35] = 0;	    cmmip[2][36] = 0;		cmmip[2][37] = 1;	cmmip[2][38] = 0;		cmmip[2][39] = 1;
	cmmip[2][40] = 1;	    cmmip[2][41] = 1;		cmmip[2][42] = 0;	cmmip[2][43] = 0;		cmmip[2][44] = 1;
	cmmip[2][45] = 1;

	cmmip[2][46] = 0.28;	cmmip[2][47] = 0.6538;	cmmip[2][48] = 0.5294;	cmmip[2][49] = 0.5094;	cmmip[2][50] = 0.5;
	cmmip[2][51] = 0.5;		cmmip[2][52] = 0;		cmmip[2][53] = 0.3333;	cmmip[2][54] = 0;		cmmip[2][55] = 0.28;
	cmmip[2][56] = 0;		cmmip[2][57] = 0.5263;	cmmip[2][58] = 0.5556;	cmmip[2][59] = 0.4528;	cmmip[2][60] = 0.5694;
	cmmip[2][61] = 0.4935;	cmmip[2][62] = 0.4918;	cmmip[2][63] = 0.3729;	cmmip[2][64] = 0;		cmmip[2][65] = 0.5882;
	cmmip[2][66] = 0.4619;	cmmip[2][67] = 0.5339;	cmmip[2][68] = 0.6667;  cmmip[2][69] = 0;

	/////맥크리

	cmmip[3][0] = 0;	    cmmip[3][1] = 0;		cmmip[3][2] = 0;	cmmip[3][3] = 0;		cmmip[3][4] = 0;
	cmmip[3][5] = 0;		cmmip[3][6] = 1;		cmmip[3][7] = 0;	cmmip[3][8] = 0;		cmmip[3][9] = 0;
	cmmip[3][10] = -0.5;	cmmip[3][11] = 0;		cmmip[3][12] = 0;	cmmip[3][13] = 0.5;		cmmip[3][14] = 0;
	cmmip[3][15] = 0.5;	    cmmip[3][16] = 0;		cmmip[3][17] = 0;	cmmip[3][18] = 0;		cmmip[3][19] = 0;
	cmmip[3][20] = 0;		cmmip[3][21] = 0;		cmmip[3][22] = 0;

	cmmip[3][23] = 0;		cmmip[3][24] = 0;
	cmmip[3][25] = 0;	    cmmip[3][26] = 0;		cmmip[3][27] = 0;	cmmip[3][28] = 1;		cmmip[3][29] = 0;
	cmmip[3][30] = 0;	    cmmip[3][31] = 0;		cmmip[3][32] = 0;	cmmip[3][33] = 0;		cmmip[3][34] = 0;
	cmmip[3][35] = 0;	    cmmip[3][36] = 1;		cmmip[3][37] = 1;	cmmip[3][38] = 0;		cmmip[3][39] = 1;
	cmmip[3][40] = 0;	    cmmip[3][41] = 0;		cmmip[3][42] = 1;	cmmip[3][43] = 0;		cmmip[3][44] = 0;
	cmmip[3][45] = 1;

	cmmip[3][46] = 0;	cmmip[3][47] = 0;	cmmip[3][48] = 0;		cmmip[3][49] = 0;	cmmip[3][50] = 0;
	cmmip[3][51] = 0;	cmmip[3][52] = 0;	cmmip[3][53] = 0;		cmmip[3][54] = 0;	cmmip[3][55] = 0;
	cmmip[3][56] = 0;	cmmip[3][57] = 0;	cmmip[3][58] = 0;		cmmip[3][59] = 0;	cmmip[3][60] = 0;
	cmmip[3][61] = 0;	cmmip[3][62] = 0;	cmmip[3][63] = 0;		cmmip[3][64] = 0;	cmmip[3][65] = 0;
	cmmip[3][66] = 0;	cmmip[3][67] = 0;	cmmip[3][68] = 0.6667;  cmmip[3][69] = 0;

	/////솔저:76

	cmmip[4][0] = 0;	    cmmip[4][1] = -1;		cmmip[4][2] = 0;	cmmip[4][3] = 0;		cmmip[4][4] = 0;
	cmmip[4][5] = 0;		cmmip[4][6] = 0;		cmmip[4][7] = -1;	cmmip[4][8] = -0.5;		cmmip[4][9] = 0;
	cmmip[4][10] = -0.5;	cmmip[4][11] = 0.5;		cmmip[4][12] = 0;	cmmip[4][13] = 0.5;		cmmip[4][14] = 1;
	cmmip[4][15] = -0.5;	cmmip[4][16] = 0;		cmmip[4][17] = 1;	cmmip[4][18] = 0;		cmmip[4][19] = 0;
	cmmip[4][20] = 0;		cmmip[4][21] = 0;		cmmip[4][22] = 0;

	cmmip[4][23] = 1;		cmmip[4][24] = 1;
	cmmip[4][25] = 1;	    cmmip[4][26] = 1;		cmmip[4][27] = 0;	cmmip[4][28] = 1;		cmmip[4][29] = 1;
	cmmip[4][30] = 0;	    cmmip[4][31] = 0;		cmmip[4][32] = 0;	cmmip[4][33] = 0;		cmmip[4][34] = 0;
	cmmip[4][35] = 0;	    cmmip[4][36] = 0;		cmmip[4][37] = 0;	cmmip[4][38] = 0;		cmmip[4][39] = 0;
	cmmip[4][40] = 0;	    cmmip[4][41] = 0;		cmmip[4][42] = 0;	cmmip[4][43] = 0;		cmmip[4][44] = 0;
	cmmip[4][45] = 0;

	cmmip[4][46] = 0;	cmmip[4][47] = 0;	cmmip[4][48] = 0;	cmmip[4][49] = 0;	cmmip[4][50] = 0;
	cmmip[4][51] = 0;	cmmip[4][52] = 0;	cmmip[4][53] = 0;	cmmip[4][54] = 0;	cmmip[4][55] = 0;
	cmmip[4][56] = 0;	cmmip[4][57] = 0;	cmmip[4][58] = 0;	cmmip[4][59] = 0;	cmmip[4][60] = 0;
	cmmip[4][61] = 0;	cmmip[4][62] = 0;	cmmip[4][63] = 0;	cmmip[4][64] = 0;	cmmip[4][65] = 0;
	cmmip[4][66] = 0;	cmmip[4][67] = 0;	cmmip[4][68] = 0;   cmmip[4][69] = 0;

	/////솜브라

	cmmip[5][0] = 0;	    cmmip[5][1] = 0;		cmmip[5][2] = 0;	cmmip[5][3] = 0;		cmmip[5][4] = 0;
	cmmip[5][5] = 0;		cmmip[5][6] = 0;		cmmip[5][7] = 0;	cmmip[5][8] = 1;		cmmip[5][9] = 0.5;
	cmmip[5][10] = -0.5;	cmmip[5][11] = -1;		cmmip[5][12] = 0.5;	cmmip[5][13] = 0;		cmmip[5][14] = 1;
	cmmip[5][15] = 0;	    cmmip[5][16] = 0.5;		cmmip[5][17] = 0;	cmmip[5][18] = -1;		cmmip[5][19] = 1;
	cmmip[5][20] = -1;		cmmip[5][21] = 0;		cmmip[5][22] = 1;

	cmmip[5][23] = 0;		cmmip[5][24] = 0;
	cmmip[5][25] = 0;	    cmmip[5][26] = 1;		cmmip[5][27] = 0;	cmmip[5][28] = 0;		cmmip[5][29] = 0;
	cmmip[5][30] = 0;	    cmmip[5][31] = 0;		cmmip[5][32] = 0;	cmmip[5][33] = 0;		cmmip[5][34] = 1;
	cmmip[5][35] = 0;	    cmmip[5][36] = 0;		cmmip[5][37] = 0;	cmmip[5][38] = 0;		cmmip[5][39] = 1;
	cmmip[5][40] = 1;	    cmmip[5][41] = 1;		cmmip[5][42] = 0;	cmmip[5][43] = 1;		cmmip[5][44] = 0;
	cmmip[5][45] = 0;

	cmmip[5][46] = 0.4706;	cmmip[5][47] = 0.4;		cmmip[5][48] = 0.4;		cmmip[5][49] = 0.357;	cmmip[5][50] = 0.3462;
	cmmip[5][51] = 0.3929;	cmmip[5][52] = 0.5263;	cmmip[5][53] = 0;		cmmip[5][54] = 0.5588;	cmmip[5][55] = 0.3438;
	cmmip[5][56] = 0.3333;	cmmip[5][57] = 0;		cmmip[5][58] = 0.4375;	cmmip[5][59] = 0.4167;	cmmip[5][60] = 0.3659;
	cmmip[5][61] = 0;		cmmip[5][62] = 0.3721;	cmmip[5][63] = 0.4889;	cmmip[5][64] = 0.3333;	cmmip[5][65] = 0.7778;
	cmmip[5][66] = 0.4837;	cmmip[5][67] = 0.5089;	cmmip[5][68] = 0.5;		cmmip[5][69] = 0;

	/////트레이서

	cmmip[6][0] = 0;	    cmmip[6][1] = 0.5;		cmmip[6][2] = -1;	cmmip[6][3] = -1;		cmmip[6][4] = 0;
	cmmip[6][5] = 0;		cmmip[6][6] = 0;		cmmip[6][7] = 0.5;	cmmip[6][8] = 0.5;		cmmip[6][9] = -1;
	cmmip[6][10] = 0.5;		cmmip[6][11] = 0.5;		cmmip[6][12] = 0.5;	cmmip[6][13] = -0.5;	cmmip[6][14] = 0;
	cmmip[6][15] = -0.5;    cmmip[6][16] = 0;		cmmip[6][17] = 0.5;	cmmip[6][18] = 0;		cmmip[6][19] = 0;
	cmmip[6][20] = 0;		cmmip[6][21] = 0;		cmmip[6][22] = -1;

	cmmip[6][23] = 0;		cmmip[6][24] = 0;
	cmmip[6][25] = 0;	    cmmip[6][26] = 0;		cmmip[6][27] = 0;	cmmip[6][28] = 0;		cmmip[6][29] = 0;
	cmmip[6][30] = 1;	    cmmip[6][31] = 0;		cmmip[6][32] = 0;	cmmip[6][33] = 1;		cmmip[6][34] = 0;
	cmmip[6][35] = 0;	    cmmip[6][36] = 0;		cmmip[6][37] = 1;	cmmip[6][38] = 0;		cmmip[6][39] = 0;
	cmmip[6][40] = 1;	    cmmip[6][41] = 0;		cmmip[6][42] = 1;	cmmip[6][43] = 0;		cmmip[6][44] = 1;
	cmmip[6][45] = 0;

	cmmip[6][46] = 0;		cmmip[6][47] = 0;		cmmip[6][48] = 0.6667;	cmmip[6][49] = 0;		cmmip[6][50] = 0;
	cmmip[6][51] = 0;		cmmip[6][52] = 0;		cmmip[6][53] = 0;		cmmip[6][54] = 0;		cmmip[6][55] = 0;
	cmmip[6][56] = 0;		cmmip[6][57] = 0;		cmmip[6][58] = 0.5;		cmmip[6][59] = 0;		cmmip[6][60] = 0.4825;
	cmmip[6][61] = 0.4375;	cmmip[6][62] = 0;		cmmip[6][63] = 0;		cmmip[6][64] = 0;		cmmip[6][65] = 0;
	cmmip[6][66] = 0;		cmmip[6][67] = 0;		cmmip[6][68] = 0.6667;  cmmip[6][69] = 0;

	/////파라

	cmmip[7][0] = 0.5;	    cmmip[7][1] = -0.5;		cmmip[7][2] = -0.5;	cmmip[7][3] = 0;		cmmip[7][4] = 1;
	cmmip[7][5] = 0;		cmmip[7][6] = -0.5;		cmmip[7][7] = 0;	cmmip[7][8] = 0;		cmmip[7][9] = -0.5;
	cmmip[7][10] = 0;		cmmip[7][11] = 0.5;		cmmip[7][12] = 0;	cmmip[7][13] = 0.5;		cmmip[7][14] = 0.5;
	cmmip[7][15] = 0;	    cmmip[7][16] = 0.5;		cmmip[7][17] = 0;	cmmip[7][18] = -0.5;	cmmip[7][19] = -0.5;
	cmmip[7][20] = 0;		cmmip[7][21] = 0;		cmmip[7][22] = 0;

	cmmip[7][23] = 0;		cmmip[7][24] = 1;
	cmmip[7][25] = 0;	    cmmip[7][26] = 0;		cmmip[7][27] = 0;	cmmip[7][28] = 0;		cmmip[7][29] = 1;
	cmmip[7][30] = 0;	    cmmip[7][31] = 0;		cmmip[7][32] = 0;	cmmip[7][33] = 0;		cmmip[7][34] = 0;
	cmmip[7][35] = 1;	    cmmip[7][36] = 1;		cmmip[7][37] = 0;	cmmip[7][38] = 0;		cmmip[7][39] = 0;
	cmmip[7][40] = 0;	    cmmip[7][41] = 1;		cmmip[7][42] = 0;	cmmip[7][43] = 0;		cmmip[7][44] = 1;
	cmmip[7][45] = 0;

	cmmip[7][46] = 0;		cmmip[7][47] = 0.4;		cmmip[7][48] = 0;		cmmip[7][49] = 0;		cmmip[7][50] = 0.5556;
	cmmip[7][51] = 0.4706;	cmmip[7][52] = 0;		cmmip[7][53] = 0;		cmmip[7][54] = 0;		cmmip[7][55] = 0.3929;
	cmmip[7][56] = 0;		cmmip[7][57] = 0.4737;	cmmip[7][58] = 0.5;		cmmip[7][59] = 0.4865;	cmmip[7][60] = 0.4722;
	cmmip[7][61] = 0.4231;	cmmip[7][62] = 0.5556;	cmmip[7][63] = 0.5;		cmmip[7][64] = 0;		cmmip[7][65] = 0.5625;
	cmmip[7][66] = 0.4619;	cmmip[7][67] = 0.5339;	cmmip[7][68] = 0;		cmmip[7][69] = 0;

	/////메이

	cmmip[8][0] = -1;	    cmmip[8][1] = -1;		cmmip[8][2] = 0;	cmmip[8][3] = 0;		cmmip[8][4] = 0.5;
	cmmip[8][5] = 1;		cmmip[8][6] = 0.5;		cmmip[8][7] = 0;	cmmip[8][8] = 0;		cmmip[8][9] = -0.5;
	cmmip[8][10] = -0.5;	cmmip[8][11] = 0;		cmmip[8][12] = -0.5; cmmip[8][13] = -0.5;	cmmip[8][14] = 0.5;
	cmmip[8][15] = 0.5;	    cmmip[8][16] = 0.5;		cmmip[8][17] = 0;	cmmip[8][18] = 0;		cmmip[8][19] = 0;
	cmmip[8][20] = 0;		cmmip[8][21] = 0;		cmmip[8][22] = 0;

	cmmip[8][23] = 0;		cmmip[8][24] = 0;
	cmmip[8][25] = 0;	    cmmip[8][26] = 0;		cmmip[8][27] = 0;	cmmip[8][28] = 1;		cmmip[8][29] = 0;
	cmmip[8][30] = 0;	    cmmip[8][31] = 0;		cmmip[8][32] = 0;	cmmip[8][33] = 0;		cmmip[8][34] = 0;
	cmmip[8][35] = 0;	    cmmip[8][36] = 0;		cmmip[8][37] = 1;	cmmip[8][38] = 0;		cmmip[8][39] = 1;
	cmmip[8][40] = 1;	    cmmip[8][41] = 0;		cmmip[8][42] = 1;	cmmip[8][43] = 0;		cmmip[8][44] = 0;
	cmmip[8][45] = 1;

	cmmip[8][46] = 0;		cmmip[8][47] = 0;	cmmip[8][48] = 0;	cmmip[8][49] = 0;	cmmip[8][50] = 0;
	cmmip[8][51] = 0;		cmmip[8][52] = 0;	cmmip[8][53] = 0;	cmmip[8][54] = 0;	cmmip[8][55] = 0;
	cmmip[8][56] = 0;		cmmip[8][57] = 0;	cmmip[8][58] = 0;	cmmip[8][59] = 0;	cmmip[8][60] = 0;
	cmmip[8][61] = 0;		cmmip[8][62] = 0;	cmmip[8][63] = 0;	cmmip[8][64] = 0;	cmmip[8][65] = 0;
	cmmip[8][66] = 0;		cmmip[8][67] = 0;	cmmip[8][68] = 0;	cmmip[8][69] = 0;

	/////바스티온

	cmmip[9][0] = -1;	    cmmip[9][1] = 0;		cmmip[9][2] = 0;	cmmip[9][3] = 0;		cmmip[9][4] = 0;
	cmmip[9][5] = -0.5;		cmmip[9][6] = 1;		cmmip[9][7] = 0.5;	cmmip[9][8] = 0.5;		cmmip[9][9] = 0;
	cmmip[9][10] = 0;		cmmip[9][11] = 1;		cmmip[9][12] = 0;	cmmip[9][13] = -0.5;	cmmip[9][14] = -1;
	cmmip[9][15] = 0;	    cmmip[9][16] = -1;		cmmip[9][17] = 0;	cmmip[9][18] = 0.5;		cmmip[9][19] = 1;
	cmmip[9][20] = 0;		cmmip[9][21] = 1;		cmmip[9][22] = 0.5;

	cmmip[9][23] = 0;		cmmip[9][24] = 0;
	cmmip[9][25] = 0;	    cmmip[9][26] = 0;		cmmip[9][27] = 0;	cmmip[9][28] = 0;		cmmip[9][29] = 0;
	cmmip[9][30] = 1;	    cmmip[9][31] = 0;		cmmip[9][32] = 0;	cmmip[9][33] = 0;		cmmip[9][34] = 0;
	cmmip[9][35] = 0;	    cmmip[9][36] = 0;		cmmip[9][37] = 1;	cmmip[9][38] = 0;		cmmip[9][39] = 1;
	cmmip[9][40] = 1;	    cmmip[9][41] = 0;		cmmip[9][42] = 1;	cmmip[9][43] = 0;		cmmip[9][44] = 0;
	cmmip[9][45] = 1;

	cmmip[9][46] = 0;		cmmip[9][47] = 0;	cmmip[9][48] = 0;	cmmip[9][49] = 0;	cmmip[9][50] = 0;
	cmmip[9][51] = 0;		cmmip[9][52] = 0;	cmmip[9][53] = 0;	cmmip[9][54] = 0;	cmmip[9][55] = 0;
	cmmip[9][56] = 0;		cmmip[9][57] = 0;	cmmip[9][58] = 0;	cmmip[9][59] = 0;	cmmip[9][60] = 0;
	cmmip[9][61] = 0;		cmmip[9][62] = 0;	cmmip[9][63] = 0;	cmmip[9][64] = 0;	cmmip[9][65] = 0;
	cmmip[9][66] = 0;		cmmip[9][67] = 0;	cmmip[9][68] = 0;	cmmip[9][69] = 0;

	/////위도우메이커

	cmmip[10][0] = 0.5;	    cmmip[10][1] = 0;		cmmip[10][2] = -0.5; cmmip[10][3] = 0.5;		cmmip[10][4] = 0.5;
	cmmip[10][5] = 0.5;		cmmip[10][6] = -0.5;	cmmip[10][7] = 0;	cmmip[10][8] = 0.5;		cmmip[10][9] = 0;
	cmmip[10][10] = 0;		cmmip[10][11] = 1;		cmmip[10][12] = 0;	cmmip[10][13] = 0;		cmmip[10][14] = 0.5;
	cmmip[10][15] = -0.5;   cmmip[10][16] = 0;		cmmip[10][17] = 0;	cmmip[10][18] = 0.5;	cmmip[10][19] = -0.5;
	cmmip[10][20] = 0.5;	cmmip[10][21] = 0.5;	cmmip[10][22] = 0.5;

	cmmip[10][23] = 0;		cmmip[10][24] = 0;
	cmmip[10][25] = 0;	    cmmip[10][26] = 1;		cmmip[10][27] = 0;	cmmip[10][28] = 1;		cmmip[10][29] = 0;
	cmmip[10][30] = 0;	    cmmip[10][31] = 0;		cmmip[10][32] = 0;	cmmip[10][33] = 0;		cmmip[10][34] = 0;
	cmmip[10][35] = 0;	    cmmip[10][36] = 0;		cmmip[10][37] = 0;	cmmip[10][38] = 0;		cmmip[10][39] = 1;
	cmmip[10][40] = 0;	    cmmip[10][41] = 0;		cmmip[10][42] = 1;	cmmip[10][43] = 1;		cmmip[10][44] = 0;
	cmmip[10][45] = 1;

	cmmip[10][46] = 0;		cmmip[10][47] = 0;	cmmip[10][48] = 0;	cmmip[10][49] = 0;	cmmip[10][50] = 0;
	cmmip[10][51] = 0;		cmmip[10][52] = 0;	cmmip[10][53] = 0;	cmmip[10][54] = 0;	cmmip[10][55] = 0;
	cmmip[10][56] = 0;		cmmip[10][57] = 0;	cmmip[10][58] = 0;	cmmip[10][59] = 0;	cmmip[10][60] = 0;
	cmmip[10][61] = 0;		cmmip[10][62] = 0;	cmmip[10][63] = 0;	cmmip[10][64] = 0;	cmmip[10][65] = 0;
	cmmip[10][66] = 0;		cmmip[10][67] = 0;	cmmip[10][68] = 0;  cmmip[10][69] = 0;

	/////정크랫

	cmmip[11][0] = 0.5;	    cmmip[11][1] = 0;		cmmip[11][2] = 0;	cmmip[11][3] = 0;		cmmip[11][4] = -0.5;
	cmmip[11][5] = 1;		cmmip[11][6] = 0.5;		cmmip[11][7] = -0.5; cmmip[11][8] = 0;		cmmip[11][9] = -1;
	cmmip[11][10] = -1;		cmmip[11][11] = 0;		cmmip[11][12] = 0;	cmmip[11][13] = -0.5;	cmmip[11][14] = -1;
	cmmip[11][15] = 0.5;	cmmip[11][16] = 0;		cmmip[11][17] = -1;	cmmip[11][18] = 0;		cmmip[11][19] = -0.5;
	cmmip[11][20] = 0;		cmmip[11][21] = -1;		cmmip[11][22] = 0;

	cmmip[11][23] = 1;		cmmip[11][24] = 0;
	cmmip[11][25] = 0;	    cmmip[11][26] = 0;		cmmip[11][27] = 0;	cmmip[11][28] = 1;		cmmip[11][29] = 0;
	cmmip[11][30] = 0;	    cmmip[11][31] = 0;		cmmip[11][32] = 0;	cmmip[11][33] = 0;		cmmip[11][34] = 0;
	cmmip[11][35] = 0;	    cmmip[11][36] = 0;		cmmip[11][37] = 1;	cmmip[11][38] = 1;		cmmip[11][39] = 0;
	cmmip[11][40] = 0;	    cmmip[11][41] = 0;		cmmip[11][42] = 0;	cmmip[11][43] = 1;		cmmip[11][44] = 1;
	cmmip[11][45] = 0;

	cmmip[11][46] = 0;		cmmip[11][47] = 0;	cmmip[11][48] = 0;	cmmip[11][49] = 0;	cmmip[11][50] = 0;
	cmmip[11][51] = 0;		cmmip[11][52] = 0;	cmmip[11][53] = 0;	cmmip[11][54] = 0;	cmmip[11][55] = 0;
	cmmip[11][56] = 0;		cmmip[11][57] = 0;	cmmip[11][58] = 0;	cmmip[11][59] = 0;	cmmip[11][60] = 0;
	cmmip[11][61] = 0;		cmmip[11][62] = 0;	cmmip[11][63] = 0;	cmmip[11][64] = 0;	cmmip[11][65] = 0;
	cmmip[11][66] = 0;		cmmip[11][67] = 0;	cmmip[11][68] = 0;  cmmip[11][69] = 0;

	/////토르비욘

	cmmip[12][0] = 0;	    cmmip[12][1] = 0;		cmmip[12][2] = 0;		cmmip[12][3] = 0;		cmmip[12][4] = 0;
	cmmip[12][5] = -0.5;	cmmip[12][6] = -0.5;	cmmip[12][7] = 0;		cmmip[12][8] = 0.5;		cmmip[12][9] = 0;
	cmmip[12][10] = 0;		cmmip[12][11] = 0;		cmmip[12][12] = 0;		cmmip[12][13] = -0.5;	cmmip[12][14] = -0.5;
	cmmip[12][15] = 1;		cmmip[12][16] = -0.5;	cmmip[12][17] = -0.5;	cmmip[12][18] = 0;		cmmip[12][19] = 0.5;
	cmmip[12][20] = 0;		cmmip[12][21] = 0;		cmmip[12][22] = 0;

	cmmip[12][23] = 0;		cmmip[12][24] = 0;
	cmmip[12][25] = 0;	    cmmip[12][26] = 0;		cmmip[12][27] = 0;	cmmip[12][28] = 0;		cmmip[12][29] = 0;
	cmmip[12][30] = 1;	    cmmip[12][31] = 0;		cmmip[12][32] = 0;	cmmip[12][33] = 0;		cmmip[12][34] = 0;
	cmmip[12][35] = 0;	    cmmip[12][36] = 0;		cmmip[12][37] = 1;	cmmip[12][38] = 0;		cmmip[12][39] = 0;
	cmmip[12][40] = 1;	    cmmip[12][41] = 1;		cmmip[12][42] = 1;	cmmip[12][43] = 0;		cmmip[12][44] = 0;
	cmmip[12][45] = 1;

	cmmip[12][46] = 0.6;	cmmip[12][47] = 0;		cmmip[12][48] = 0;	cmmip[12][49] = 0;		cmmip[12][50] = 0;
	cmmip[12][51] = 0;		cmmip[12][52] = 0;		cmmip[12][53] = 0;	cmmip[12][54] = 0;		cmmip[12][55] = 0;
	cmmip[12][56] = 0;		cmmip[12][57] = 0;		cmmip[12][58] = 0;	cmmip[12][59] = 0;		cmmip[12][60] = 0.6098;
	cmmip[12][61] = 0;		cmmip[12][62] = 0.5862;	cmmip[12][63] = 0;	cmmip[12][64] = 0.58;	cmmip[12][65] = 0;
	cmmip[12][66] = 0;		cmmip[12][67] = 0;		cmmip[12][68] = 0;	cmmip[12][69] = 0;

	/////한조

	cmmip[13][0] = 0.5;	    cmmip[13][1] = 0;		cmmip[13][2] = 0;		cmmip[13][3] = -0.5;	cmmip[13][4] = -0.5;
	cmmip[13][5] = 0;		cmmip[13][6] = 0.5;		cmmip[13][7] = -0.5;	cmmip[13][8] = 0.5;		cmmip[13][9] = 0.5;
	cmmip[13][10] = 0;		cmmip[13][11] = 0.5;	cmmip[13][12] = 0.5;	cmmip[13][13] = 0;		cmmip[13][14] = 0;
	cmmip[13][15] = 0;		cmmip[13][16] = 0;		cmmip[13][17] = -1;		cmmip[13][18] = -1;		cmmip[13][19] = 0;
	cmmip[13][20] = -1;		cmmip[13][21] = -0.5;	cmmip[13][22] = -1;

	cmmip[13][23] = 0;		cmmip[13][24] = 0;
	cmmip[13][25] = 0;	    cmmip[13][26] = 0;		cmmip[13][27] = 0;	cmmip[13][28] = 0;		cmmip[13][29] = 0;
	cmmip[13][30] = 1;	    cmmip[13][31] = 0;		cmmip[13][32] = 0;	cmmip[13][33] = 0;		cmmip[13][34] = 0;
	cmmip[13][35] = 0;	    cmmip[13][36] = 0;		cmmip[13][37] = 1;	cmmip[13][38] = 0;		cmmip[13][39] = 1;
	cmmip[13][40] = 1;	    cmmip[13][41] = 0;		cmmip[13][42] = 1;	cmmip[13][43] = 0;		cmmip[13][44] = 0;
	cmmip[13][45] = 1;

	cmmip[13][46] = 0;		cmmip[13][47] = 0;		cmmip[13][48] = 0;		cmmip[13][49] = 0;		cmmip[13][50] = 0;
	cmmip[13][51] = 0;		cmmip[13][52] = 0;		cmmip[13][53] = 0;		cmmip[13][54] = 0;		cmmip[13][55] = 0;
	cmmip[13][56] = 0;		cmmip[13][57] = 0;		cmmip[13][58] = 0.3846;	cmmip[13][59] = 0;	cmmip[13][60] = 0;
	cmmip[13][61] = 0;		cmmip[13][62] = 0;		cmmip[13][63] = 0;		cmmip[13][64] = 0;		cmmip[13][65] = 0;
	cmmip[13][66] = 0;		cmmip[13][67] = 0;		cmmip[13][68] = 0.4;	cmmip[13][69] = 0;

	/////D.VA

	cmmip[14][0] = 0;	    cmmip[14][1] = -1;		cmmip[14][2] = 0;		cmmip[14][3] = 0;		cmmip[14][4] = -1;
	cmmip[14][5] = -1;		cmmip[14][6] = 0;		cmmip[14][7] = -0.5;	cmmip[14][8] = -0.5;	cmmip[14][9] = 1;
	cmmip[14][10] = -0.5;	cmmip[14][11] = 1;		cmmip[14][12] = 0.5;	cmmip[14][13] = 0;		cmmip[14][14] = 0;
	cmmip[14][15] = -0.5;	cmmip[14][16] = 0;		cmmip[14][17] = 0;		cmmip[14][18] = 0;		cmmip[14][19] = 0;
	cmmip[14][20] = -1;		cmmip[14][21] = 0;		cmmip[14][22] = 1;

	cmmip[14][23] = 0;		cmmip[14][24] = 0;
	cmmip[14][25] = 1;	    cmmip[14][26] = 1;		cmmip[14][27] = 0;	cmmip[14][28] = 0;		cmmip[14][29] = 0;
	cmmip[14][30] = 0;	    cmmip[14][31] = 1;		cmmip[14][32] = 0;	cmmip[14][33] = 0;		cmmip[14][34] = 0;
	cmmip[14][35] = 1;	    cmmip[14][36] = 0;		cmmip[14][37] = 0;	cmmip[14][38] = 0;		cmmip[14][39] = 0;
	cmmip[14][40] = 0;	    cmmip[14][41] = 0;		cmmip[14][42] = 1;	cmmip[14][43] = 0;		cmmip[14][44] = 1;
	cmmip[14][45] = 0;

	cmmip[14][46] = 0.4906;	cmmip[14][47] = 0.4681;	cmmip[14][48] = 0.5161;		cmmip[14][49] = 0.5;	cmmip[14][50] = 0.4412;
	cmmip[14][51] = 0.55;	cmmip[14][52] = 0.6364;	cmmip[14][53] = 0.3103;		cmmip[14][54] = 0.7826;	cmmip[14][55] = 0.2821;
	cmmip[14][56] = 0.4167;	cmmip[14][57] = 0.5238;	cmmip[14][58] = 0.5362;		cmmip[14][59] = 0.4032;	cmmip[14][60] = 0.5169;
	cmmip[14][61] = 0.456;	cmmip[14][62] = 0.5122;	cmmip[14][63] = 0.4554;		cmmip[14][64] = 0.4571;	cmmip[14][65] = 0.5581;
	cmmip[14][66] = 0.504;	cmmip[14][67] = 0.4762;	cmmip[14][68] = 0.3333;	    cmmip[14][69] = 0;

	/////라인하르트

	cmmip[15][0] = 0;	    cmmip[15][1] = -1;		cmmip[15][2] = -1;		cmmip[15][3] = -0.5;	cmmip[15][4] = 0.5;
	cmmip[15][5] = 0;		cmmip[15][6] = 0.5;		cmmip[15][7] = 0;		cmmip[15][8] = -0.5;	cmmip[15][9] = 0;
	cmmip[15][10] = 0.5;	cmmip[15][11] = -0.5;	cmmip[15][12] = -1;		cmmip[15][13] = 0;		cmmip[15][14] = 0.5;
	cmmip[15][15] = 0;		cmmip[15][16] = 0;		cmmip[15][17] = 0;		cmmip[15][18] = 1;		cmmip[15][19] = -0.5;
	cmmip[15][20] = 0;		cmmip[15][21] = -0.5;	cmmip[15][22] = -0.5;

	cmmip[15][23] = 0;		cmmip[15][24] = 0;
	cmmip[15][25] = 0;	    cmmip[15][26] = 0;		cmmip[15][27] = 0;	cmmip[15][28] = 0;		cmmip[15][29] = 0;
	cmmip[15][30] = 1;	    cmmip[15][31] = 0;		cmmip[15][32] = 0;	cmmip[15][33] = 0;		cmmip[15][34] = 0;
	cmmip[15][35] = 0;	    cmmip[15][36] = 0;		cmmip[15][37] = 1;	cmmip[15][38] = 0;		cmmip[15][39] = 1;
	cmmip[15][40] = 0;	    cmmip[15][41] = 0;		cmmip[15][42] = 1;	cmmip[15][43] = 0;		cmmip[15][44] = 1;
	cmmip[15][45] = 1;

	cmmip[15][46] = 0.5588;	cmmip[15][47] = 0.5758;	cmmip[15][48] = 0.5484;		cmmip[15][49] = 0.4419;		cmmip[15][50] = 0.375;
	cmmip[15][51] = 0.5294;	cmmip[15][52] = 0.7059;	cmmip[15][53] = 0.3333;		cmmip[15][54] = 0.7333;		cmmip[15][55] = 0.1818;
	cmmip[15][56] = 0;		cmmip[15][57] = 0;		cmmip[15][58] = 0.4889;		cmmip[15][59] = 0.4;		cmmip[15][60] = 0.488;
	cmmip[15][61] = 0.3488;	cmmip[15][62] = 0.5;	cmmip[15][63] = 0.55;		cmmip[15][64] = 0;		cmmip[15][65] = 0.6111;
	cmmip[15][66] = 0.5181;	cmmip[15][67] = 0;		cmmip[15][68] = 0;			cmmip[15][69] = 0;

	/////로드호그

	cmmip[16][0] = 1;	    cmmip[16][1] = -1;		cmmip[16][2] = -1;		cmmip[16][3] = 0;		cmmip[16][4] = 0;
	cmmip[16][5] = -0.5;	cmmip[16][6] = 0;		cmmip[16][7] = -0.5;	cmmip[16][8] = -0.5;	cmmip[16][9] = 1;
	cmmip[16][10] = 0;		cmmip[16][11] = 0;		cmmip[16][12] = 0.5;	cmmip[16][13] = 0;		cmmip[16][14] = 0;
	cmmip[16][15] = 0;		cmmip[16][16] = 0;		cmmip[16][17] = 0;		cmmip[16][18] = -0.5;	cmmip[16][19] = 0;
	cmmip[16][20] = 0;		cmmip[16][21] = 1;		cmmip[16][22] = 0;

	cmmip[16][23] = 0;		cmmip[16][24] = 0;
	cmmip[16][25] = 0;	    cmmip[16][26] = 1;		cmmip[16][27] = 0;	cmmip[16][28] = 0;		cmmip[16][29] = 0;
	cmmip[16][30] = 0;	    cmmip[16][31] = 0;		cmmip[16][32] = 1;	cmmip[16][33] = 0;		cmmip[16][34] = 0;
	cmmip[16][35] = 0;	    cmmip[16][36] = 0;		cmmip[16][37] = 0;	cmmip[16][38] = 0;		cmmip[16][39] = 0;
	cmmip[16][40] = 1;	    cmmip[16][41] = 1;		cmmip[16][42] = 0;	cmmip[16][43] = 0;		cmmip[16][44] = 1;
	cmmip[16][45] = 1;

	cmmip[16][46] = 0.1667;	cmmip[16][47] = 0.5517;	cmmip[16][48] = 0.3953;	cmmip[16][49] = 0.6364;	cmmip[16][50] = 0.3448;
	cmmip[16][51] = 0.3636;	cmmip[16][52] = 0;		cmmip[16][53] = 0;		cmmip[16][54] = 0.6667;	cmmip[16][55] = 0.2581;
	cmmip[16][56] = 0.4737;	cmmip[16][57] = 0;		cmmip[16][58] = 0.4861;	cmmip[16][59] = 0.439;	cmmip[16][60] = 0.5588;
	cmmip[16][61] = 0.5357;	cmmip[16][62] = 0.4773;	cmmip[16][63] = 0.3143;	cmmip[16][64] = 0.3214;	cmmip[16][65] = 0.6429;
	cmmip[16][66] = 0.4751;	cmmip[16][67] = 0.4948;	cmmip[16][68] = 0.6;	cmmip[16][69] = 0.4782;

	/////윈스턴

	cmmip[17][0] = 1;	    cmmip[17][1] = 0;		cmmip[17][2] = 1;		cmmip[17][3] = 0;		cmmip[17][4] = -1;
	cmmip[17][5] = 0;		cmmip[17][6] = -0.5;	cmmip[17][7] = 0;		cmmip[17][8] = 0;		cmmip[17][9] = 0;
	cmmip[17][10] = 0;		cmmip[17][11] = 1;		cmmip[17][12] = 0.5;	cmmip[17][13] = 1;		cmmip[17][14] = 0;
	cmmip[17][15] = 0;		cmmip[17][16] = 0;		cmmip[17][17] = 0;		cmmip[17][18] = 0;		cmmip[17][19] = 0;
	cmmip[17][20] = 1;		cmmip[17][21] = 1;		cmmip[17][22] = -0.5;

	cmmip[17][23] = 1;		cmmip[17][24] = 1;
	cmmip[17][25] = 0;	    cmmip[17][26] = 0;		cmmip[17][27] = 0;	cmmip[17][28] = 1;		cmmip[17][29] = 1;
	cmmip[17][30] = 0;	    cmmip[17][31] = 0;		cmmip[17][32] = 1;	cmmip[17][33] = 0;		cmmip[17][34] = 0;
	cmmip[17][35] = 1;	    cmmip[17][36] = 0;		cmmip[17][37] = 1;	cmmip[17][38] = 0;		cmmip[17][39] = 0;
	cmmip[17][40] = 0;	    cmmip[17][41] = 0;		cmmip[17][42] = 0;	cmmip[17][43] = 0;		cmmip[17][44] = 0;
	cmmip[17][45] = 0;

	cmmip[17][46] = 0.5106;	cmmip[17][47] = 0.5238;	cmmip[17][48] = 0.5;	cmmip[17][49] = 0.48;	cmmip[17][50] = 0.4118;
	cmmip[17][51] = 0.5526;	cmmip[17][52] = 0.6667;	cmmip[17][53] = 0.2963;	cmmip[17][54] = 0.7234;	cmmip[17][55] = 0.2973;
	cmmip[17][56] = 0.5882;	cmmip[17][57] = 0.6;	cmmip[17][58] = 0.5616;	cmmip[17][59] = 0.4375;	cmmip[17][60] = 0.541;
	cmmip[17][61] = 0.4537;	cmmip[17][62] = 0.5161;	cmmip[17][63] = 0.4583;	cmmip[17][64] = 0.4419;	cmmip[17][65] = 0.5789;
	cmmip[17][66] = 0.4871;	cmmip[17][67] = 0.4976;	cmmip[17][68] = 0.5;	cmmip[17][69] = 0.5;

	/////자리야

	cmmip[18][0] = -1;	    cmmip[18][1] = 0;		cmmip[18][2] = -1;		cmmip[18][3] = 0;		cmmip[18][4] = 0;
	cmmip[18][5] = 1;		cmmip[18][6] = 0;		cmmip[18][7] = 0.5;		cmmip[18][8] = 0;		cmmip[18][9] = -0.5;
	cmmip[18][10] = 1;		cmmip[18][11] = 0;		cmmip[18][12] = 0;		cmmip[18][13] = 1;		cmmip[18][14] = 0;
	cmmip[18][15] = -1;		cmmip[18][16] = 0.5;	cmmip[18][17] = 0;		cmmip[18][18] = 0;		cmmip[18][19] = 0;
	cmmip[18][20] = 0;		cmmip[18][21] = 0;		cmmip[18][22] = 0;

	cmmip[18][23] = 0;		cmmip[18][24] = 0;
	cmmip[18][25] = 1;	    cmmip[18][26] = 0;		cmmip[18][27] = 0;	cmmip[18][28] = 0;		cmmip[18][29] = 0;
	cmmip[18][30] = 1;	    cmmip[18][31] = 0;		cmmip[18][32] = 0;	cmmip[18][33] = 0;		cmmip[18][34] = 0;
	cmmip[18][35] = 0;	    cmmip[18][36] = 0;		cmmip[18][37] = 1;	cmmip[18][38] = 1;		cmmip[18][39] = 0;
	cmmip[18][40] = 0;	    cmmip[18][41] = 0;		cmmip[18][42] = 1;	cmmip[18][43] = 0;		cmmip[18][44] = 0;
	cmmip[18][45] = 1;

	cmmip[18][46] = 0.5094;	cmmip[18][47] = 0.5439;	cmmip[18][48] = 0.4722;	cmmip[18][49] = 0.5077;	cmmip[18][50] = 0.4318;
	cmmip[18][51] = 0.5263;	cmmip[18][52] = 0.5806;	cmmip[18][53] = 0.3;	cmmip[18][54] = 0.6875;	cmmip[18][55] = 0.3111;
	cmmip[18][56] = 0.4762;	cmmip[18][57] = 0.4737;	cmmip[18][58] = 0.4943;	cmmip[18][59] = 0.4304;	cmmip[18][60] = 0.5596;
	cmmip[18][61] = 0.4667;	cmmip[18][62] = 0.5258;	cmmip[18][63] = 0.4556;	cmmip[18][64] = 0.439;	cmmip[18][65] = 0.5405;
	cmmip[18][66] = 0.4895;	cmmip[18][67] = 0.4973;	cmmip[18][68] = 0.5;	cmmip[18][69] = 0.5124;

	/////루시우

	cmmip[19][0] = -1;	    cmmip[19][1] = 0;		cmmip[19][2] = -0.5;	cmmip[19][3] = 0;		cmmip[19][4] = 0;
	cmmip[19][5] = -1;		cmmip[19][6] = 0;		cmmip[19][7] = 0.5;		cmmip[19][8] = 0;		cmmip[19][9] = -1;
	cmmip[19][10] = 0;		cmmip[19][11] = 0.5;	cmmip[19][12] = -0.5;	cmmip[19][13] = 0;		cmmip[19][14] = 0;
	cmmip[19][15] = 0.5;	cmmip[19][16] = 0;		cmmip[19][17] = 0;		cmmip[19][18] = 0;		cmmip[19][19] = 0;
	cmmip[19][20] = 0;		cmmip[19][21] = 0;		cmmip[19][22] = 0;

	cmmip[19][23] = 0;		cmmip[19][24] = 0;
	cmmip[19][25] = 0;	    cmmip[19][26] = 0;		cmmip[19][27] = 0;	cmmip[19][28] = 0;		cmmip[19][29] = 1;
	cmmip[19][30] = 0;	    cmmip[19][31] = 1;		cmmip[19][32] = 1;	cmmip[19][33] = 0;		cmmip[19][34] = 0;
	cmmip[19][35] = 0;	    cmmip[19][36] = 0;		cmmip[19][37] = 1;	cmmip[19][38] = 1;		cmmip[19][39] = 0;
	cmmip[19][40] = 0;	    cmmip[19][41] = 1;		cmmip[19][42] = 0;	cmmip[19][43] = 0;		cmmip[19][44] = 0;
	cmmip[19][45] = 0;

	cmmip[19][46] = 0;		cmmip[19][47] = 0;		cmmip[19][48] = 0;		cmmip[19][49] = 0.6;	cmmip[19][50] = 0.3846;
	cmmip[19][51] = 0;		cmmip[19][52] = 0;		cmmip[19][53] = 0;		cmmip[19][54] = 0;		cmmip[19][55] = 0;
	cmmip[19][56] = 0;		cmmip[19][57] = 0;		cmmip[19][58] = 0.48;	cmmip[19][59] = 0.4615;	cmmip[19][60] = 0.4194;
	cmmip[19][61] = 0.4167;	cmmip[19][62] = 0.5;	cmmip[19][63] = 0.6;	cmmip[19][64] = 0.4167;	cmmip[19][65] = 0.625;
	cmmip[19][66] = 0;		cmmip[19][67] = 0;		cmmip[19][68] = 0.3333; cmmip[19][69] = 0;

	/////메르시

	cmmip[20][0] = 0.5;	    cmmip[20][1] = 0;		cmmip[20][2] = 0;		cmmip[20][3] = 0;		cmmip[20][4] = 0;
	cmmip[20][5] = 1;		cmmip[20][6] = 0;		cmmip[20][7] = 0;		cmmip[20][8] = 0;		cmmip[20][9] = 0;
	cmmip[20][10] = -0.5;	cmmip[20][11] = 0;		cmmip[20][12] = 0;		cmmip[20][13] = 1;		cmmip[20][14] = 1;
	cmmip[20][15] = 0;		cmmip[20][16] = -1;		cmmip[20][17] = -1;		cmmip[20][18] = 0;		cmmip[20][19] = 0;
	cmmip[20][20] = 0;		cmmip[20][21] = 0;		cmmip[20][22] = 0;

	cmmip[20][23] = 1;		cmmip[20][24] = 0;
	cmmip[20][25] = 0;	    cmmip[20][26] = 0;		cmmip[20][27] = 0;	cmmip[20][28] = 1;		cmmip[20][29] = 0;
	cmmip[20][30] = 0;	    cmmip[20][31] = 0;		cmmip[20][32] = 0;	cmmip[20][33] = 0;		cmmip[20][34] = 1;
	cmmip[20][35] = 0;	    cmmip[20][36] = 0;		cmmip[20][37] = 1;	cmmip[20][38] = 0;		cmmip[20][39] = 0;
	cmmip[20][40] = 0;	    cmmip[20][41] = 1;		cmmip[20][42] = 1;	cmmip[20][43] = 0;		cmmip[20][44] = 0;
	cmmip[20][45] = 0;

	cmmip[20][46] = 0;	cmmip[20][47] = 0;	cmmip[20][48] = 0;	cmmip[20][49] = 0;	cmmip[20][50] = 0;
	cmmip[20][51] = 0;	cmmip[20][52] = 0;	cmmip[20][53] = 0;	cmmip[20][54] = 0;	cmmip[20][55] = 0;
	cmmip[20][56] = 0;	cmmip[20][57] = 0;	cmmip[20][58] = 0;	cmmip[20][59] = 0;	cmmip[20][60] = 0;
	cmmip[20][61] = 0;	cmmip[20][62] = 0;	cmmip[20][63] = 0;	cmmip[20][64] = 0;	cmmip[20][65] = 0;
	cmmip[20][66] = 0;	cmmip[20][67] = 0;	cmmip[20][68] = 0;  cmmip[20][69] = 0;

	/////시메트라

	cmmip[21][0] = 0;	    cmmip[21][1] = -0.5;	cmmip[21][2] = 0;		cmmip[21][3] = 0;		cmmip[21][4] = 0;
	cmmip[21][5] = 0;		cmmip[21][6] = 0;		cmmip[21][7] = 0;		cmmip[21][8] = 0;		cmmip[21][9] = -1;
	cmmip[21][10] = -0.5;	cmmip[21][11] = 1;		cmmip[21][12] = 0;		cmmip[21][13] = 0.5;	cmmip[21][14] = 0;
	cmmip[21][15] = 0.5;	cmmip[21][16] = 0;		cmmip[21][17] = 0;		cmmip[21][18] = 0;		cmmip[21][19] = 0;
	cmmip[21][20] = 0;		cmmip[21][21] = 0;		cmmip[21][22] = 0;

	cmmip[21][23] = 1;		cmmip[21][24] = 1;
	cmmip[21][25] = 0;	    cmmip[21][26] = 0;		cmmip[21][27] = 0;	cmmip[21][28] = 0;		cmmip[21][29] = 1;
	cmmip[21][30] = 0;	    cmmip[21][31] = 0;		cmmip[21][32] = 0;	cmmip[21][33] = 0;		cmmip[21][34] = 0;
	cmmip[21][35] = 0;	    cmmip[21][36] = 0;		cmmip[21][37] = 1;	cmmip[21][38] = 1;		cmmip[21][39] = 0;
	cmmip[21][40] = 0;	    cmmip[21][41] = 0;		cmmip[21][42] = 1;	cmmip[21][43] = 0;		cmmip[21][44] = 0;
	cmmip[21][45] = 0;

	cmmip[21][46] = 0.5106;	cmmip[21][47] = 0.5208;	cmmip[21][48] = 0.4915;	cmmip[21][49] = 0.5714;	cmmip[21][50] = 0.3929;
	cmmip[21][51] = 0.5;	cmmip[21][52] = 0.64;	cmmip[21][53] = 0.2692;	cmmip[21][54] = 0.6774;	cmmip[21][55] = 0.2222;
	cmmip[21][56] = 0.3571;	cmmip[21][57] = 0.4667;	cmmip[21][58] = 0.5588;	cmmip[21][59] = 0.4231;	cmmip[21][60] = 0.5181;
	cmmip[21][61] = 0.4235;	cmmip[21][62] = 0.5147;	cmmip[21][63] = 0.4571;	cmmip[21][64] = 0.4828;	cmmip[21][65] = 0.5833;
	cmmip[21][66] = 0.4997;	cmmip[21][67] = 0.4914;	cmmip[21][68] = 0.5;	cmmip[21][69] = 0.1429;

	/////아나

	cmmip[22][0] = -1;	    cmmip[22][1] = 0;		cmmip[22][2] = 0;		cmmip[22][3] = 0;		cmmip[22][4] = 0;
	cmmip[22][5] = -1;		cmmip[22][6] = 1;		cmmip[22][7] = 0;		cmmip[22][8] = 0;		cmmip[22][9] = -0.5;
	cmmip[22][10] = -0.5;	cmmip[22][11] = 0;		cmmip[22][12] = 0;		cmmip[22][13] = 1;		cmmip[22][14] = -1;
	cmmip[22][15] = 0.5;	cmmip[22][16] = 1;		cmmip[22][17] = 0.5;	cmmip[22][18] = 0;		cmmip[22][19] = 0;
	cmmip[22][20] = 0;		cmmip[22][21] = 0;		cmmip[22][22] = 0;

	cmmip[22][23] = 1;		cmmip[22][24] = 1;
	cmmip[22][25] = 0;	    cmmip[22][26] = 0;		cmmip[22][27] = 0;	cmmip[22][28] = 0;		cmmip[22][29] = 0;
	cmmip[22][30] = 0;	    cmmip[22][31] = 0;		cmmip[22][32] = 0;	cmmip[22][33] = 0;		cmmip[22][34] = 0;
	cmmip[22][35] = 0;	    cmmip[22][36] = 0;		cmmip[22][37] = 0;	cmmip[22][38] = 0;		cmmip[22][39] = 1;
	cmmip[22][40] = 0;	    cmmip[22][41] = 1;		cmmip[22][42] = 1;	cmmip[22][43] = 0;		cmmip[22][44] = 1;
	cmmip[22][45] = 0;

	cmmip[22][46] = 0.4167;	cmmip[22][47] = 0.375;	cmmip[22][48] = 0.55;	cmmip[22][49] = 0.4516;	cmmip[22][50] = 0.4667;
	cmmip[22][51] = 0.6471;	cmmip[22][52] = 0;		cmmip[22][53] = 0;		cmmip[22][54] = 0.7059;	cmmip[22][55] = 0.3158;
	cmmip[22][56] = 0;		cmmip[22][57] = 0;		cmmip[22][58] = 0.6154;	cmmip[22][59] = 0.5806;	cmmip[22][60] = 0.5208;
	cmmip[22][61] = 0.451;	cmmip[22][62] = 0.475;	cmmip[22][63] = 0.44;	cmmip[22][64] = 0;	cmmip[22][65] = 0;
	cmmip[22][66] = 0.476;	cmmip[22][67] = 0.5287;	cmmip[22][68] = 0;		cmmip[22][69] = 0.4804;

	/////젠야타

	int map;
	int ally[5];
	int enemy[6];


	int k = 0;

	srand((unsigned)time(NULL));


	while (k < num_train) {

		int r = 0;
		int s = 0;

		map = rand() % 24 + 46;

		while (r < 5) {

			if (r == 0) {

				ally[r] = rand() % 23 + 23;
			}
			else if (r != 0) {

				ally[r] = rand() % 23 + 23;

				for (int j = 0; j < r; j++) {
					if (ally[j] == ally[r]) {
						r--;
					}
				}

			}
			r++;

		}



		while (s < 6) {


			if (s == 0) {

				enemy[s] = rand() % 23;
			}
			else if (s != 0) {

				enemy[s] = rand() % 23;

				for (int j = 0; j < s; j++) {
					if (enemy[j] == enemy[s]) {
						s--;
					}
				}

			}
			s++;

		}


		input[k][0] = map;
		for (int i = 1; i < 6; i++) {
			input[k][i] = ally[i - 1];
		}

		for (int i = 6; i < 12; i++) {
			input[k][i] = enemy[i - 6];
		}

		for (int i = 0; i < num_output; i++) {

			target_output[k][i] = cmmip[i][map];

			for (int j = 0; j < 5; j++) {

				target_output[k][i] += (cmmip[i][ally[j]] * 0.2);
			}

			for (int j = 0; j < 6; j++) {

				target_output[k][i] += (cmmip[i][enemy[j]] / 6);
			}

			target_output[k][i] = (target_output[k][i] + 1) / 3;
		}
		/*
		for (int i = 0; i < num_input; i++) {
		printf("%d번째 입력 %d : %4f\n", k, i, input[k][i]);
		}
		for (int i = 0; i < num_output; i++) {
		printf("%d번째 출력 %d : %4f\n", k, i, target_output[k][i]);
		}
		*/

		k++;
	}
	printf("초기 반복학습 중입니다. 잠시만 기다려주세요.\n");
	FNN->Train(num_train, learning_rate, input, target_output);

	for (int i = 0; i < num_train; i++){
		double *output = new double[num_output];

		FNN->Test(input[i], output);

		/*
		for (int j = 0; j < num_output; j+=10){
		printf("출력 :  %lf", output[j]);
		}
		printf("\n");
		*/

		delete[] output;
	}

	while (1) {

		int *max;

		max = (int*)malloc(sizeof(int)* 23);
		int rank = 0;

		printf("감시기지 지브롤터(공격) : 1\n");
		printf("감시기지 지브롤터(수비) : 2\n");
		printf("도라도(공격) : 3\n");
		printf("도라도(수비) : 4\n");
		printf("66번 국도(공격) : 5\n");
		printf("66번 국도(수비) : 6\n");
		printf("볼스카야 인더스트리(공격) : 7\n");
		printf("볼스카야 인더스트리(수비) : 8\n");
		printf("아누비스 신전(공격) : 9\n");
		printf("아누비스 신전(수비) : 10\n");
		printf("하나무라(공격) : 11\n");
		printf("하나무라(수비) : 12\n");
		printf("눔바니(공격) : 13\n");
		printf("눔바니(수비) : 14\n");
		printf("왕의 길(공격) : 15\n");
		printf("왕의 길(수비) : 16\n");
		printf("할리우드(공격) : 17\n");
		printf("할리우드(수비) : 18\n");
		printf("아이헨발데(공격) : 19\n");
		printf("아이헨발데(수비) : 20\n");
		printf("네팔 : 21\n");
		printf("리장타워 : 22\n");
		printf("오아시스 : 23\n");
		printf("일리오스 : 24\n");

		printf("현재 맵을 입력하세요(위에 맵에 해당된 숫자를 입력 : ");
		scanf_s("%lf", &input[0][0]);

		Watcher::run();

		for (int i = 1; i < 7; i++) {
			input[0][i] = FNN->ae[i - 1] + 23;
		}
		for (int i = 7; i < 12; i++) {
			input[0][i] = FNN->aa[i - 7];
		}

		for (int i = 0; i < num_input; i++) {
			printf("input[0][%d] = %lf\n", i, input[0][i]);
		}

		for (int i = 0; i < num_output; i++) {

			target_output[0][i] = cmmip[i][(int)input[0][0] + 45]; //map

			for (int j = 0; j < 5; j++) {

				target_output[0][i] += (cmmip[i][(int)input[0][j + 1]] * 0.2); //enemy heroes
			}

			for (int j = 0; j < 6; j++) {

				target_output[0][i] += (cmmip[i][(int)input[0][j + 6]] / 6); //ally heroes
			}

			target_output[0][i] = (target_output[0][i] + 1) / 3;
		}

		FNN->Train(1, learning_rate, input, target_output);
		double *output = new double[num_output];
		FNN->Test(input[0], output);

		printf("겐지 :  \t%lf \n", output[0]);
		printf("리퍼 :  \t%lf \n", output[1]);
		printf("맥크리 :  \t%lf \n", output[2]);
		printf("솔저:76 :  \t%lf \n", output[3]);
		printf("솜브라 :  \t%lf \n", output[4]);
		printf("트레이서 :  \t%lf \n", output[5]);
		printf("파라 :  \t%lf \n", output[6]);
		printf("메이 :  \t%lf \n", output[7]);
		printf("바스티온 :  \t%lf \n", output[8]);
		printf("위도우메이커 : \t%lf \n", output[9]);
		printf("정크랫 :  \t%lf \n", output[10]);
		printf("토르비욘 :  \t%lf \n", output[11]);
		printf("한조 :  \t%lf \n", output[12]);
		printf("D.VA :  \t%lf \n", output[13]);
		printf("라인하르트 :  \t%lf \n", output[14]);
		printf("로드호그 :  \t%lf \n", output[15]);
		printf("윈스턴 :  \t%lf \n", output[16]);
		printf("자리야 :  \t%lf \n", output[17]);
		printf("루시우 :  \t%lf \n", output[18]);
		printf("메르시 :  \t%lf \n", output[19]);
		printf("시메트라 :  \t%lf \n", output[20]);
		printf("아나 :  \t%lf \n", output[21]);
		printf("젠야타 :  \t%lf \n", output[22]);

		for (int i = 0; i < num_output; i++) {
			rank = 1;
			for (int j = 0; j < num_output; j++) {
				if (output[i] < output[j]) {
					rank++;
				}
				max[i] = rank;
			}
		}

		printf("현재 플레이하기 가장 적합한 영웅 순위입니다.\n");
		for (int i = 1; i < 6; i++) {
			for (int j = 0; j < num_output; j++) {
				if (max[j] == i){
					printf("%d위 : %s\n", i, Heroname[j]);
				}
			}
			
		}
		

		_sleep(5000);

		for (int i = 0; i < num_output; i++) {
			max[i] = 0;
		}
		
		delete[] output;
	}
	for (int i = 0; i < num_train; i++){
		delete[] input[i];
		delete[] target_output[i];
	}
	delete[] input;
	delete[] target_output;
	delete FNN;
}