﻿#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>

using namespace std;

class Feedforward_Neural_Networks{
private:
	int num_input;
	int num_hidden;
	int num_output;

	double **hidden_weight;
	double **output_weight;

	void Compute_Output(double input[], double hidden[], double output[]){
		for (int i = 0; i < num_hidden; i++)
		{
			double sum = 0;

			for (int j = 0; j < num_input; j++)
			{
				sum += input[j] * hidden_weight[i][j];
			}
			sum += hidden_weight[i][num_input];
			hidden[i] = 1 / (1 + exp(-sum));
		}
		for (int i = 0; i < num_output; i++)
		{
			double sum = 0;

			for (int j = 0; j < num_hidden; j++)
			{
				sum += hidden[j] * output_weight[i][j];
			}
			sum += output_weight[i][num_hidden];
			output[i] = 1 / (1 + exp(-sum));
		}
	}
public:
	Feedforward_Neural_Networks(int num_input, int num_hidden, int num_output){
		this->num_input = num_input;
		this->num_hidden = num_hidden;
		this->num_output = num_output;

		output_weight = new double*[num_output];
		for (int i = 0; i < num_output; i++){
			output_weight[i] = new double[num_hidden + 1];
		}
		hidden_weight = new double*[num_hidden];
		for (int i = 0; i < num_hidden; i++){
			hidden_weight[i] = new double[num_input + 1];
		}
	}

	~Feedforward_Neural_Networks(){
		for (int i = 0; i < num_output; i++){
			delete[] output_weight[i];
		}
		for (int i = 0; i < num_hidden; i++){
			delete[] hidden_weight[i];
		}
		delete[] hidden_weight;
		delete[] output_weight;
	}


	void Test(double input[], double output[]){
		double *hidden = new double[num_hidden];

		Compute_Output(input, hidden, output);

		delete[] hidden;
	}

	void Train(int num_train, double learning_rate, double **input, double **target_output)
	{
		int num_epoch = 0;
		int max_epoch = 100000;

		double *hidden = new double[num_hidden];
		double *hidden_derivative = new double[num_hidden];
		double *output = new double[num_output];
		double *output_derivative = new double[num_output];

		srand(0);
		for (int i = 0; i < num_hidden; i++){
			for (int j = 0; j < num_input + 1; j++){
				hidden_weight[i][j] = 0.2 * rand() / RAND_MAX - 0.1;
			}
		}
		for (int i = 0; i < num_output; i++){
			for (int j = 0; j < num_hidden + 1; j++){
				output_weight[i][j] = 0.2 * rand() / RAND_MAX - 0.1;
			}
		}

		do{
			double error = 0;

			for (int i = 0; i < num_train; i++){
				Compute_Output(input[i], hidden, output);

				// 출력미분값 계산
				for (int j = 0; j < num_output; j++){
					output_derivative[j] = learning_rate * (output[j] - target_output[i][j]) * (1 - output[j]) * output[j];
				}

				// 출력가중치 조정
				for (int j = 0; j < num_output; j++){
					for (int k = 0; k < num_hidden; k++){
						output_weight[j][k] -= output_derivative[j] * hidden[k];
					}
					output_weight[j][num_hidden] -= output_derivative[j];
				}

				// 은닉미분값 계산
				for (int j = 0; j < num_hidden; j++){
					double sum = 0;

					for (int k = 0; k < num_output; k++){
						sum += output_derivative[k] * output_weight[k][j];
					}
					hidden_derivative[j] = sum * (1 - hidden[j]) * hidden[j];
				}

				// 은닉가중치 조정
				for (int j = 0; j < num_hidden; j++){
					for (int k = 0; k < num_input; k++){
						hidden_weight[j][k] -= hidden_derivative[j] * input[i][k];
					}
					hidden_weight[j][num_input] -= hidden_derivative[j];
				}

				// 오차 계산
				for (int j = 0; j < num_output; j++){
					error += 0.5 * (output[j] - target_output[i][j]) * (output[j] - target_output[i][j]);
				}
			}
			if (num_epoch % 1000 == 0){
				cout << "반복횟수 : " << num_epoch << " 오차 : " << error << endl;
			}
		} while (num_epoch++ < max_epoch);

		delete[] hidden;
		delete[] output;
	}

};

void quickSort(double arr[], int size)
{
	double pivot = arr[0];
	int cursor = 0;
	for (int i = 1; i < size; i++)
	{
		if (pivot > arr[i])
		{
			cursor++;
			swap(arr[cursor], arr[i]);
		}
	}
	swap(arr[0], arr[cursor]);
	if (cursor > 0)
	{
		quickSort(arr, cursor);
	}
	if (cursor + 1 < size - 1)
	{
		quickSort(arr + cursor + 1, size - cursor - 1);
	}
}



string* StringSplit(string strTarget, string strTok)
{
	int nCutPos;
	int nIndex = 0;
	string* strResult = new string[100];



	while ((nCutPos = strTarget.find_first_of(strTok)) != strTarget.npos)
	{
		if (nCutPos > 0)
		{
			strResult[nIndex++] = strTarget.substr(0, nCutPos);
		}
		strTarget = strTarget.substr(nCutPos + 1);
	}

	if (strTarget.length() > 0)
	{
		strResult[nIndex++] = strTarget.substr(0, nCutPos);
	}

	return strResult;
}

/*

void main(){
int num_input = 2;
int num_hidden = 2;
int num_output = 2;
int num_train = 4;

double learning_rate = 0.1;

double **input = new double*[num_train];
double **target_output = new double*[num_train];

Feedforward_Neural_Networks *FNN = new Feedforward_Neural_Networks(num_input, num_hidden, num_output);

for (int i = 0; i < num_train; i++){
input[i] = new double[num_input];
target_output[i] = new double[num_output];
}

input[0][0] = 0; input[0][1] = 0;
input[1][0] = 0; input[1][1] = 1;
input[2][0] = 1; input[2][1] = 0;
input[3][0] = 1; input[3][1] = 1;

target_output[0][0] = 0; target_output[0][1] = 0;
target_output[1][0] = 0; target_output[1][1] = 1;
target_output[2][0] = 0; target_output[2][1] = 1;
target_output[3][0] = 1; target_output[3][1] = 0;

FNN->Train(num_train, learning_rate, input, target_output);

for (int i = 0; i < num_train; i++){
double *output = new double[num_output];

cout << "입력 " << i << " ";
for (int j = 0; j < num_input; j++){
cout << input[i][j] << " ";
}
FNN->Test(input[i], output);

cout << "출력 ";
for (int j = 0; j < num_output; j++){

cout << output[j];
}
cout << endl;

delete[] output;
}
for (int i = 0; i < num_train; i++){
delete[] input[i];
delete[] target_output[i];
}
delete[] input;
delete[] target_output;
delete FNN;
}

*/

int main()
{
	int num_input = 6;
	int num_output = 45;
	int num_hidden = 30;
	int num_train = 500;

	double learning_rate = 0.001;

	ifstream in("test.txt");

	double **input = new double*[num_train];
	double **target_output = new double*[num_train];

	Feedforward_Neural_Networks *FNN = new Feedforward_Neural_Networks(num_input, num_hidden, num_output);

	for (int i = 0; i < num_train; i++)
	{
		input[i] = new double[num_input]; //input[100][6]
		target_output[i] = new double[num_output]; //target_output[100][6]
	}

	int num = 0;

	string a;

	if (in.is_open()) {
		while (!in.eof()) {
			getline(in, a);
			num++;
		}
	}
	else {
		cout << "can't file this file!" << endl;
	}

	int **c = new int*[num];
	string *s = new string[num];
	string **split = new string*[num];

	for (int i = 0; i < num; i++)
	{
		c[i] = new int[7];
	}

	if (in.is_open()) {
		in.seekg(0, ios::beg);
		for (int i = 0; i < num; i++)
		{
			getline(in, s[i]);
			//cout << s[i] << endl;
		}
	}

	cout << num << endl;

	for (int i = 0; i < num; i++)
	{
		split[i] = StringSplit(s[i], "\t");

		for (int j = 1; j < 8; j++)
		{
			c[i][j - 1] = atoi(split[i][j].c_str());
		}

	}

	// define Input c [ i = number of trails ] [ j = number of ball ]

	int k = 0; //num train

	srand((unsigned)time(NULL));

	int i = 0;

	/*
	double temp_output[45];

	while (i < num_output)
	{
		if (i == 0)
		{
			temp_output[i] = 1 / 45; // 1 to 45
		}
		i++;
	}

	quickSort(temp_output, num_output);
	*/

	while (k < num_train)
	{
		int r = 0;

		int in_rand = rand() % num;

		for (int i = 0; i < num_output; i++)
		{
			target_output[k][i] = 0;
		}

		for (int i = 0; i < num_input; i++)
		{
			input[k][i] = c[in_rand][i];
			target_output[k][c[in_rand][i]] = 1.0 / 45.0;
		}

		//cout << "회차 : " << atoi(split[in_rand][0].c_str()) << " " << (num - in_rand) << endl;


		//quickSort(target_output[k], num_output);
	
		//cout << "입력 : " << input[k][0] << "\t" << input[k][1] << "\t" << input[k][2] << "\t" << input[k][3] << "\t" << input[k][4] << "\t" << input[k][5] << endl;
		//cout << "출력 : " << target_output[k][0] << "\t" << target_output[k][1] << "\t" << target_output[k][2] << "\t" << target_output[k][3] << "\t" << target_output[k][4] << "\t" << target_output[k][5] << endl;

		k++;
	}

	FNN->Train(num_train, learning_rate, input, target_output);

	for (int i = 0; i < 5; i++){
		double *output = new double[num_output];

		cout << "입력 : ";
		for (int j = 0; j < num_input; j++){
			cout << input[i][j] << "\t";
		}
		cout << endl;
		FNN->Test(input[i], output);

		cout << " ";

		int *max;

		max = (int*)malloc(sizeof(int)* 45);
		int rank = 0;

		for (int i = 0; i < num_output; i++) {
			rank = 1;
			for (int j = 0; j < num_output; j++) {
				if (output[i] < output[j]) {
					rank++;
				}
				max[i] = rank;
			}
		}

		//quickSort(output, num_output);

		cout << "출력 : ";

		for (int i = 1; i < 7; i++) {
			for (int j = 0; j < num_output; j++) {
				if (max[j] == i){
					cout << "출력값 높은 순 " << i << ": " << output[j] << "번호 : " << j << endl;
				}
			}

		}

		cout << endl;

		delete[] output;
	}

	/*

	for (int i = 0; i < num; i++)
	{
	delete split[i];
	delete c[i];
	}

	for (int i = 0; i < num_train; i++){
	delete[] input[i];
	delete[] target_output[i];
	}

	delete[] split;
	delete[] s;
	delete[] c;
	delete[] input;
	delete[] target_output;

	*/

	return 0;
}

//개선 사항 1. 출력층은 45개여야 한다. 